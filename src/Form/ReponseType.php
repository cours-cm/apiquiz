<?php

namespace App\Form;

use App\Entity\Reponse;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ReponseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('text', TextareaType::class, ["label"=>"Texte de la réponse"])
            ->add('isGood', CheckboxType::class, ["label"=>"Bonne réponse", "required"=>false, "row_attr"=>["class"=>"form-switch"]])
            ->remove('imageName')
            ->remove('updatedAt')
        ;
        if($options["newReponseAlone"]){
            $builder->add('question', EntityType::class, [
                "class"=>"App\Entity\Question",
                "choice_label"=>"intitule",
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reponse::class,
            'newReponseAlone'=>false,
        ]);
    }
}
