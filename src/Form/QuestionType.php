<?php

namespace App\Form;

use App\Entity\Question;
use App\Form\ReponseType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class QuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('intitule', TextareaType::class, ["label"=>"Intitulé de la question"] )
            ->remove('imageName')
            ->remove('updatedAt')
            ->add('imageFile', FileType::class, [
                "label"=>"Image",
                "required"=>false,
            ])
            ->add("reponses", CollectionType::class, [
                "entry_type"=>ReponseType::class,
                "allow_add"=>true,
                "allow_delete"=>true,
                "by_reference"=>false,
                "label"=>false,
            ])
            // ->add("reponses", null, ["choice_label"=>"text", "label"=>"Réponses"])
        ;
        if($options["newQuestionAlone"]){
            $builder->add('quiz', EntityType::class, [
                "class"=>"App\Entity\Quiz",
                "choice_label"=>"nom",
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Question::class,
            'newQuestionAlone'=>false,
        ]);
    }
}
