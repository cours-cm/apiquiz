<?php

namespace App\Form;

use App\Entity\Quiz;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class QuizType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, ["required"=>true])
            ->add('description', TextareaType::class, ["required"=>false])
            // ->add('imageName')
            // ->add('updatedAt')
            // ->add('questions', EntityType::class, [
            //     "class"=>"App\Entity\Question",
            //     "multiple"=>true,
            //     // "expanded"=>true,
            //     "choice_label"=>"intitule",
            //     "attr"=>["class"=>"select2"],
            //     "required"=>false,
            // ])
            ->add('questions', CollectionType::class, [
                "entry_type"=>QuestionType::class,
                "allow_add"=>true,
                "allow_delete"=>true,
                "by_reference"=>false,
                "label"=>false,
            ])
            ->add('imageFile', FileType::class, [
                "required"=>false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Quiz::class,
        ]);
    }
}
